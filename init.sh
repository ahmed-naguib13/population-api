#create our app database
echo "create database symfony" | mysql -u root
# go to the app folder
cd /var/www
# we need to create the tables, indexes
# @todo use migrations
php app/console doctrine:schema:update --force
# loop through the data files and run the command for them
for filename in data/*.txt; do
    php app/console population:import $(basename $filename)
done
