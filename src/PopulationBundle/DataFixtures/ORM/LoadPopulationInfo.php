<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PopulationBundle\Entity\PopulationInfo;

class LoadPopulationInfo implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $cityInfo = new PopulationInfo('cairo', '1,761', 'Egypt', '9,000');
        $manager->persist($cityInfo);

        $cityInfo = new PopulationInfo('copenhagen', '616', 'Denmark', '2,000');
        $manager->persist($cityInfo);

        $manager->flush();
    }
}