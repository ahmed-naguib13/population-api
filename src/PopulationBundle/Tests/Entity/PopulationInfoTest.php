<?php

namespace PopulationBundle\Tests\Entity;

use PopulationBundle\Entity\PopulationInfo;

class DefaultControllerTest extends \PHPUnit_Framework_TestCase
{
    public function testGetFloatDensity()
    {
        $cityInfo = new PopulationInfo('cairo', '1,761', 'Egypt', '9,000');
        $floatVal = $cityInfo->getFloatDensity();
        $this->assertEquals(1761, $floatVal);
    }

    public function testGetFloatArea()
    {
        $cityInfo = new PopulationInfo('cairo', '1,761', 'Egypt', '9,000');
        $floatVal = $cityInfo->getFloatArea();
        $this->assertEquals(9000, $floatVal);
    }
}
