<?php

namespace PopulationBundle\Tests\Command;

use PopulationBundle\Tests\BaseAPITestCase;
use PopulationBundle\Command\DataImporterCommand;
use Symfony\Component\Console\Tester\CommandTester;
/**
 * Class DataImprterCommandTest
 * @package PopulationBundle\Tests\Command
 */
class DataImporterCommandTest extends BaseAPITestCase
{
    /**
     * testExecute
     */
    public function testExecute()
    {
        $application = self::getApplication();
        $importCommand = new DataImporterCommand();
        $application->add($importCommand);

        $command = $application->find('population:import');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            array(
                'fileName' => 'GR.txt',
                'dir' => __DIR__ . '/../Mocks'
            )
        );

        $this->assertContains('[success] 2 cities imported', $commandTester->getDisplay());
    }

    /**
     * testExecute
     */
    public function testWrongFileExecute()
    {
        $application = self::getApplication();
        $importCommand = new DataImporterCommand();
        $application->add($importCommand);

        $command = $application->find('population:import');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            array(
                'fileName' => 'sssss.txt',
                'dir' => __DIR__ . '/../Mocks'
            )
        );

        $this->assertContains('File not found', $commandTester->getDisplay());
    }
}
