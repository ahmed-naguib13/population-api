<?php

namespace PopulationBundle\Tests\Controller;

use PopulationBundle\Tests\BaseAPITestCase;

/**
 * Class DefaultControllerTest
 * @package PopulationBundle\Tests\Controller
 */
class DefaultControllerTest extends BaseAPITestCase
{
    /**
     * test for calculate action
     */
    public function testCalculate()
    {
        $application = self::getApplication();
        $client = $application->getKernel()->getContainer()->get('test.client');
        $crawler = $client->request('GET', '/api/calculate/Copenhagen/50');
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('estimatedPopulation', $response);
        $this->assertEquals('30800', $response['estimatedPopulation']);
    }

    /**
     * test for calculate action
     */
    public function testWrongRadius()
    {
        $application = self::getApplication();
        $client = $application->getKernel()->getContainer()->get('test.client');
        $crawler = $client->request('GET', '/api/calculate/Copenhagen/50000');
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('message', $response);
        $this->assertEquals('Invalid radius value', $response['message']);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    /**
     * test for calculate action
     */
    public function testCalculateWrongCity()
    {
        $application = self::getApplication();
        $client = $application->getKernel()->getContainer()->get('test.client');
        $crawler = $client->request('GET', '/api/calculate/Amsterdam/50');
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('message', $response);
        $this->assertEquals('City not found', $response['message']);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}
