<?php

namespace PopulationBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;

/**
 * Class BaseAPITestCase
 * @package PopulationBundle\Tests
 */
class BaseAPITestCase extends WebTestCase
{
    /**
     * @var
     */
    protected static $application;

    /**
     * prepare the database and load the fixtures
     */
    protected function setUp()
    {
        self::runCommand('doctrine:database:create');
        self::runCommand('doctrine:schema:update --force');
        self::runCommand('doctrine:fixtures:load --purge-with-truncate ');
    }

    /**
     * @param $command
     * @return mixed
     */
    protected static function runCommand($command)
    {
        $command = sprintf('%s -q -n', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    /**
     * @return Application
     */
    protected static function getApplication()
    {
        if (null === self::$application) {
            $client = static::createClient();

            self::$application = new Application($client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }
}