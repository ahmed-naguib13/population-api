<?php

namespace PopulationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Finder\Finder;
use PopulationBundle\Entity\PopulationInfo;

class DataImporterCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('population:import')
            ->setDescription('Import data to the database')
            ->addArgument(
                'fileName',
                InputArgument::OPTIONAL,
                'Specify the file path'
            )->addArgument(
                'dir',
                InputArgument::OPTIONAL,
                'Specify the directory'
            );

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // set the output styling
        $style = new OutputFormatterStyle('red', 'yellow', array('bold', 'blink'));
        $output->getFormatter()->setStyle('fire', $style);

        // read the filename from input and try to find the file
        $fileName = $input->getArgument('fileName');
        $directory = $input->getArgument('dir');
        if (empty($directory)) {
            $directory = __DIR__ . "/../../../data";
        }
        $finder = new Finder();
        $files = $finder->files()->name($fileName)->in($directory);

        if ($files->count() == 0) {
            $output->writeln("<fire>File not found, please make sure the name is correct and the file is located in data folder</>");

            return;
        }

        // get the entity manager we need it to save the new data
        $em = $this->getContainer()->get('doctrine')->getManager();
        $logger = $this->getContainer()->get('logger');

        $processed = 0;
        // go through the file found line by line and add new entities
        foreach ($files as $file) {
            if (($handle = fopen($file->getRealPath(), "r")) !== FALSE) {
                while (($line = fgets($handle, $file->getSize())) !== FALSE) {
                    try {
                        $parts = explode(' ', $line);
                        // sometimes the first item is a ranking id, we can skip it
                        if (is_numeric($parts[0])) {
                            unset($parts[0]); // remove item at index 0
                            $parts = array_values($parts); // 'reindex' array
                        }
                        $country = $parts[0];
                        $city = strtolower($parts[1]);
                        $area = $parts[4];
                        $density = $parts[7];
                        $cityInfo = new PopulationInfo($city, $density, $country, $area);
                        $em->persist($cityInfo);
                        $processed++;
                    } catch (\Exception $e) {
                        $logger->error($e->getMessage());
                        $output->writeln("<fire>Could not process entry $city .. will skip</>");
                    }
                }
                // try to save it to the database
                try {
                    $em->flush();
                } catch (\Exception $e) {
                    $logger->error($e->getMessage());
                    $output->writeln("<fire>Could not save entry .. will skip</>");
                }
                fclose($handle);
            }
        }

        if ($processed) {
            $output->writeln("[success] $processed cities imported</>");

            return;
        }
    }
}