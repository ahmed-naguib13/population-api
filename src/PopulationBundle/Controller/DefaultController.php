<?php

namespace PopulationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DefaultController
 * @package PopulationBundle\Controller
 */
class DefaultController extends Controller
{

    /**
     * @param $data
     * @param int $status
     * @return JsonResponse
     * @throws \Exception
     */
    private function getJsonResponse($data, $status = 200)
    {
        $response = new JsonResponse();
        $response->setData($data);
        $response->setStatusCode($status);

        return $response;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('PopulationBundle:Default:index.html.twig');
    }

    /**
     * @param $city
     * @param $radius
     * @param Request $request
     * @return JsonResponse
     */
    public function calculateAction($city, $radius, Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $info = $em->getRepository('PopulationBundle:PopulationInfo')
            ->findOneByCity(strtolower($city));
        if (empty($info)) {
            return $this->getJsonResponse(array('message' => 'City not found'), 404);
        }

        if($info->getFloatArea() < $radius){
            return $this->getJsonResponse(array('message' => 'Invalid radius value'), 400);
        }

        $population = $info->getFloatDensity() * $radius;

        $result = array(
            'city' => ucfirst($info->getCity()),
            'estimatedPopulation' => $population,
            'country' => $info->getCountry(),
        );
        return $this->getJsonResponse($result);
    }
}
