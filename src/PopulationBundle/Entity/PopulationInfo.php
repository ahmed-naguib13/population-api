<?php

namespace PopulationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PopulationInfo
 *
 * @ORM\Table(name="population_info",indexes={@ORM\Index(name="city_idx", columns={"city"})})
 * @ORM\Entity(repositoryClass="PopulationBundle\Repository\PopulationInfoRepository")
 */
class PopulationInfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, unique=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="density", type="string", length=255)
     */
    private $density;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=255)
     */
    private $area;

    /**
     * PopulationInfo constructor.
     * @param string $city
     * @param string $density
     * @param string $country
     * @param string $area
     */
    public function __construct($city, $density, $country, $area)
    {
        $this->city = $city;
        $this->density = $density;
        $this->country = $country;
        $this->area = $area;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return PopulationInfo
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set density
     *
     * @param string $density
     * @return PopulationInfo
     */
    public function setDensity($density)
    {
        $this->density = $density;

        return $this;
    }

    /**
     * Get density
     *
     * @return string
     */
    public function getDensity()
    {
        return $this->density;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return PopulationInfo
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param string $area
     */
    public function setArea($area)
    {
        $this->area = $area;
    }

    /**
     * @return float
     */
    public function getFloatDensity()
    {
        return $this->getFloatVal($this->getDensity());
    }


    /**
     * @return float
     */
    public function getFloatArea()
    {
        return $this->getFloatVal($this->getArea());
    }

    /**
     * @param $item
     * @return float
     */
    private function getFloatVal($item)
    {
        $formatted = str_replace(',', '', $item);

        return floatval($formatted);
    }
}
