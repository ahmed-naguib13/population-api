Population API
---------------

This is a simple api that returns the estimated population for a specific city within a given radius

Example:
----------------

	GET /api/calculate/{city}/{radius}
	    city: required string ex.mumbai
	    radius: integer ex.15
	response:
	Code: 200
	Body:
	    {
            "city": "Mumbai",
            "estimatedPopulation": 900000,
            "country": "India"
        }

Approach:
------------------------
- the database is filled with data for major cities in a country. For each city we have the population density ( number of people per Km)

Thus, when given a specific radius we can estimate the population according to the following:

	estimatedPopulation = DensityPerKm * Radius

- Of course this is the naive approach because it assumes that all parts of the city have exactly the same amount of people.

*Note*: Unfortunately I didn't have enough time to get better data to produce more accurate results.

Features:
-------------
- Everything is in the database no extra calls during the request
- The approach is straightforward and easy to understand
- 10 countries are supported : 
Egypt, Germany , France, Russia, Nigeria, Turkey, Poland, Italy , Japan and Sweden

Installation
------------------
- Prerequisites:
	- You need to have a machine where you have composer, vagrant , vitrualbox installed
- clone the repository

		git clone git@gitlab.com:ahmed-naguib13/population-api.git
		cd population-api
		composer install
		vagrant up

- Add the following to /etc/hosts

		192.168.68.8  symfony.dev

- Boom, everything should be ready now you only need to http://symfony.dev/ in your browser and you should see the app running.
- Depending on your machine , the speed may be affected because virtual box is a bit slow.

Running the tests
----------------------

	vagrant ssh
	cd /var/www
	./vendor/phpunit/phpunit/phpunit -c app/

If All green ... Wohoo

Extension & World Domination
---------------
Mainly the 10 supported countries you will find them in a .txt file for each country in the *data/* folder
So, If you have a new country or even more cities to be added you should create a new txt file add it to the *data/* folder then :

	vagrant ssh
	php app/console population:import NewCountry.txt

Data Source
-----------
http://www.demographia.com/db-worldua.pdf
After formatting it a bit :
https://dl.dropboxusercontent.com/u/77010156/all-countries.txt